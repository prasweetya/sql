# No.1 Membuat Database
create database mysql;

# No.2 Membuat Table dalam database
create table users(
    id int auto_increment,
    name varchar(255),
    email varchar(255),
    password varchar(255),
    primary key(id)
);

create table categories(
    id int auto_increment,
    name varchar(255),
    primary key(id)
);

create table items(
    id int auto_increment,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    primary key(id),
    foreign key(category_id) references categories(id)
);

# untuk melihat table yg telah dibuat
describe users;
describe categories;
describe items;

#3 Memasukkan Data pada Table
insert into users (name, email, password) values ('John Doe','john@doe.com','john123');
insert into users (name, email, password) values ('Jane Doe','jane@doe.com','jenita123');

insert into categories (name) values ('gadget');
insert into categories (name) values ('cloth');
insert into categories (name) values ('men');
insert into categories (name) values ('women');
insert into categories (name) values ('branded');

insert into items (name, description, price, stock, category_id) values ('Sumsang b50','hape keren dari merek sumsang','4000000','100','1');
insert into items (name, description, price, stock, category_id) values ('Uniklooh','baju keren dari brand ternama','500000','50','2');
insert into items (name, description, price, stock, category_id) values ('IMHO Watch','jam tangan anak yang jujur banget','2000000','10','1');

#untuk melihat table yang telah dibuat
select * from users;
select * from categories;
select * from items;

#4 Mengambil Data dari Database
(a)
select id, name, email from users;
(b)
select * from items where price > 1000000;
select * from items where name like %sang%;
(c)
select items.name, items. description, items.price, items.stock, items.category_id, categories.name from items join categories on items.category_id = categories.id;

5. Mengubah Data dari database
update items set price = '2500000' where name = Sumsang b50;

#untuk melihat table yang telah dibuat
select * from items;